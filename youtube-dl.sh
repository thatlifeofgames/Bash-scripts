#!/bin/bash

rm -rf /home/personal/yt-dl/tmp/*

# Manual (individual videos) archival

/usr/local/bin/yt-dlp \
--dateafter 20050101 \
--download-archive ./downloaded-manual.txt \
--batch-file ./manual.txt \
--no-overwrites \
--ignore-errors \
--continue \
--add-metadata \
--write-description \
--write-thumbnail \  
--write-info-json \
--geo-bypass \
--no-write-playlist-metafiles \
--force-ipv6 \
-o "/root/yt-dl/tmp/Archive/%(title)s-%(upload_date)s" \
--exec 'rclone move /root/yt-dl/tmp/ yt-dl:/ --progress --delete-empty-src-dirs --config /root/.config/rclone/rclone.conf --progress --retries 50 && echo'

echo 'Finished manual, moving to playlists...'

# Playlists

/usr/local/bin/yt-dlp \
--dateafter 20050101 \
--download-archive ./downloaded-playlists.txt \
--batch-file ./playlists.txt \
--no-overwrites \
--ignore-errors \
--continue \
--add-metadata \
--write-description \
--write-thumbnail \  
--write-info-json \
--no-write-playlist-metafiles \
--geo-bypass \
--force-ipv6 \
-o "/root/yt-dl/tmp/Playlists/%(playlist_title)s/%(title)s-%(uploader)s-%(upload_date)s.%(ext)s" \
--exec 'rclone move /root/yt-dl/tmp/ yt-dl:/ --progress --delete-empty-src-dirs --config /root/.config/rclone/rclone.conf --progress --retries 50 && echo'

echo 'Finished playlists, moving to channels...'

# Channels

/usr/local/bin/yt-dlp \
--dateafter 20050101 \
--download-archive ./downloaded-channels.txt \
--batch-file ./channels.txt \
--no-overwrites \
--ignore-errors \
--continue \
--add-metadata \
--write-description \
--write-thumbnail \
--write-info-json \
--geo-bypass \
--no-write-playlist-metafiles \
--force-ipv6 \
-o "/root/yt-dl/tmp/Channels/%(uploader)s/%(title)s-%(upload_date)s.%(ext)s" \
--exec 'rclone move /root/yt-dl/tmp/ yt-dl:/ --progress --delete-empty-src-dirs --config /root/.config/rclone/rclone.conf --progress --retries 50 && echo'

echo "Finished channels... All done. Ended at $(date)"
