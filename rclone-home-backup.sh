################################
###                          ###
###    Main backup script    ###
### Last modified 29/01/2022 ###
###                          ###
################################

###
### Chris data
###

# Purge backup data that is now 30 days old, including 4 additional days in case of downtime
/usr/bin/rclone purge gchris:/OldData/$(date --date="30 days ago" +%m)/$(date --date="30 days ago" +%d) --config /root/.config/rclone/rclone.conf --retries 2
/usr/bin/rclone purge gchris:/OldData/$(date --date="31 days ago" +%m)/$(date --date="31 days ago" +%d) --config /root/.config/rclone/rclone.conf --retries 1
/usr/bin/rclone purge gchris:/OldData/$(date --date="32 days ago" +%m)/$(date --date="32 days ago" +%d) --config /root/.config/rclone/rclone.conf --retries 1
/usr/bin/rclone purge gchris:/OldData/$(date --date="33 days ago" +%m)/$(date --date="33 days ago" +%d) --config /root/.config/rclone/rclone.conf --retries 1
/usr/bin/rclone purge gchris:/OldData/$(date --date="34 days ago" +%m)/$(date --date="34 days ago" +%d) --config /root/.config/rclone/rclone.conf --retries 1
/usr/bin/rclone purge gchris:/OldData/$(date --date="35 days ago" +%m)/$(date --date="35 days ago" +%d) --config /root/.config/rclone/rclone.conf --retries 1

# Delete previous month folder if there are no days remaining
outputDaysC=$( /usr/bin/rclone lsf gchris:/OldData/$(date --date="35 days ago" +%m) --config /root/.config/rclone/rclone.conf )
if [ -z "$outputDaysC" ]
then
   /usr/bin/rclone purge gchris:/OldData/$(date --date="35 days ago" +%m) --config /root/.config/rclone/rclone.conf --retries 3
   echo "Previous month folder has been purged due to no items remaining"
fi

# If for whatever reason the month folder from 2 months ago still exists, delete it and all data within. This may happen due to the script not being ran for a long time
/usr/bin/rclone purge gchris:/OldData/$(date --date="60 days ago" +%m) --config /root/.config/rclone/rclone.conf --retries 0

/usr/bin/rclone sync /Main/Chris/ gchris:/Chris/\
 --drive-stop-on-upload-limit \
 --fast-list \
 --transfers 8 \
 --create-empty-src-dirs \
 --retries 25 \
 --config /root/.config/rclone/rclone.conf \
 --backup-dir gchris:/OldData/Chris/$(date +%m)/$(date +%d)/ \
 --retries-sleep 30s \
 --tpslimit-burst 50 \
 --progress

###
### Linux iso data
###

# Purge backup data that is now 30 days old, including 4 additional days in case of downtime
/usr/bin/rclone purge gbackup:/OldData/$(date --date="30 days ago" +%m)/$(date --date="30 days ago" +%d) --config /root/.config/rclone/rclone.conf --retries 2
/usr/bin/rclone purge gbackup:/OldData/$(date --date="31 days ago" +%m)/$(date --date="31 days ago" +%d) --config /root/.config/rclone/rclone.conf --retries 1
/usr/bin/rclone purge gbackup:/OldData/$(date --date="32 days ago" +%m)/$(date --date="32 days ago" +%d) --config /root/.config/rclone/rclone.conf --retries 1
/usr/bin/rclone purge gbackup:/OldData/$(date --date="33 days ago" +%m)/$(date --date="33 days ago" +%d) --config /root/.config/rclone/rclone.conf --retries 1
/usr/bin/rclone purge gbackup:/OldData/$(date --date="34 days ago" +%m)/$(date --date="34 days ago" +%d) --config /root/.config/rclone/rclone.conf --retries 1
/usr/bin/rclone purge gbackup:/OldData/$(date --date="35 days ago" +%m)/$(date --date="35 days ago" +%d) --config /root/.config/rclone/rclone.conf --retries 1

# Delete previous month folder if there are no days remaining
outputDaysT=$( /usr/bin/rclone lsf gbackup:/OldData/$(date --date="35 days ago" +%m) --config /root/.config/rclone/rclone.conf )
if [ -z "$outputDaysT" ]
then
   /usr/bin/rclone purge gbackup:/OldData/$(date --date="35 days ago" +%m) --config /root/.config/rclone/rclone.conf --retries 3
   echo "Previous month folder has been purged due to no items remaining"
fi

# If for whatever reason the month folder from 2 months ago still exists, delete it and all data within. This may happen due to the script not being ran for a long time
/usr/bin/rclone purge gbackup:/OldData/$(date --date="60 days ago" +%m) --config /root/.config/rclone/rclone.conf --retries 0

/usr/bin/rclone sync /Main/LinuxISOs/ gbackup:/LinuxISOs/\
 --drive-stop-on-upload-limit \
 --fast-list \
 --transfers 1 \
 --retries 25 \
 --create-empty-src-dirs \
 --config /root/.config/rclone/rclone.conf \
 --backup-dir gbackup:/OldData/LinuxISOs/$(date +%m)/$(date +%d)/ \
 --retries-sleep 30s \
 --tpslimit-burst 50 \
 --progress
